# A Bot for the Coding Blocks Slack community.

## Running the bot

Make sure you have an environmental variable set with the bot 
auth token called `AUTH_TOKEN`.

### With docker

Build the image and create a container:

    docker build . -t slackbot
    docker run -d -e PYTHONBUFFERED=0 -e AUTH_TOKEN=$AUTH_TOKEN slackbot

### Without docker

Setup a python 3.6 virtualenv using your favourite virtualenv tool 
(I recommend virtualenvwrapper).

You can also setup the `AUTH_TOKEN` variable to only be instantiated
when turning on the virtualenv, instead of polluting your global
environment.

Install the requirements:

    pip install -r requirements.txt

Then run the `bot.py` file:

    python bot.py