import nltk


def dialogue_act_features(text):
    features = {}

    for word in nltk.word_tokenize(text):
        features['contains({})'.format(word.lower())] = True

    return features
